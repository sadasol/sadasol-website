<section class="module bg-dark-60 parallax-bg" id="contact" data-background="/assets/images/backgrounds/bg-1.jpg">
          <div class="container">

            <div class="row">
              
                <div class="col-sm-4">
                  <h3 class="font-alt">Contact <?= $SYSTEM_VARIABLES['company-name'];?></h3>
                  <p>Address: <?= $SYSTEM_VARIABLES['contact']['address'];?></p>
                  <p>Phone: <?= $SYSTEM_VARIABLES['contact']['phone'];?></p>
                  <p>Email: <a href="#">&nbsp;<?= $SYSTEM_VARIABLES['contact']['email'];?></a></p>
                  <p>
                  </p>
                </div>
              
              <div class="col-sm-6 col-sm-offset-1">
                <form id="contactForm" role="form" method="post" action="/lib/contact.php">
                  <div class="form-group">
                    <label class="sr-only" for="name">Name</label>
                    <input class="form-control" type="text" id="name" name="name" placeholder="Your Name*" required="required" data-validation-required-message="Please enter your name."/>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="email">Email</label>
                    <input class="form-control" type="email" id="email" name="email" placeholder="Your Email*" required="required" data-validation-required-message="Please enter your email address."/>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" rows="7" id="message" name="message" placeholder="Your Message*" required="required" data-validation-required-message="Please enter your message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="text-center">
                    <button class="btn btn-block btn-round btn-d" id="cfsubmit" type="submit">Submit</button>
                  </div>
                </form>
                <div class="ajax-response font-alt" id="contactFormResponse"></div>
              </div>
            </div>
          </div>
        </section>