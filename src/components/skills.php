<?php

class Skills {
    
    private $title;
    private $skills;
    
    function __construct($title, $skills){
        $this->title = $title;
        $this->skills = $skills;
    }

    public function getHTMLBox(){
        $skills = implode(" / ", $this->skills);
        echo 
<<<HTML
            <div class="menu">
                <div class="row">
                <div class="col-sm-12">
                    <h4 class="menu-title font-alt">{$this->title}</h4>
                    <div class="menu-detail font-serif">{$skills}</div>
                </div>
                </div>
            </div>
HTML;
    }


}

?>