<div class="row landing-image-text">
    <div class="col-sm-12 text-justify">
        <p>This is a search application that enables the users to search in certain chapters within books. Our team implemented both the frontend and the backend parts of this application. The search application includes different features including the following:</p>
        <ul>
            <li>Constrained search:
                <p>The user can limit the search scope to a certain chapter name. For instance, the user can search for “education” within Introduction chapters of all available books. The user can choose the chapter name from a dropdown menu or he can prefix his search text with a chapter name in uppercase and colon (e.g. INTRODUCTION:education).</p>
            </li>
            <li>Boolean search:
                <p>The application is designed to accept complex search queries using Boolean search operations such as AND, OR and NOT operations. A special-purpose search text parser is designed to parse and translate the logic of Boolean queries. Here are of search scenarios with Boolean logic: </p>   
                <ol>
                    <li> <code> INTRODUCTION:cancer AND CONCLUSION:drugs</code> <br/>
                        Look for “cancer” in Introduction chapter and “drugs” in Conclusion.
                    </li>
                    <li> <code> RESULTS:education OR CONCLUSION:enhancement</code> <br/>
                        Search for “education” in Results chapter or “enhancement” in Conclusion chapter.
                    </li>
                    <li> <code> DISCUSSION:(cancer NOT breast)</code> <br/>
                        Search for books that contains the word “cancer” but does not contain the word “breast” in Discussion chapter
                    </li>
                </ol>
                <br/>
                <p>The user can use any combination of the search operations to construct his intended logic. In addition, the user can search for matches that occur in all chapters (e.g. ALL:something) or matches that occur in any chapter (e.g. ANY:anything).</p>
            </li>
            <li>Facet navigation:
                <p>Facets clusters the search results into a set of categories that summaries the search results. In this application, we supported the faceted search with different facets such as the chapter names, the authors and the year. The user can click on any of the facet’s values to limit the search to that value.</p>
            </li>
            <li>Results snippets and Full view:
                <p>The results are shown in snippets that shows a sample of the chapter’s content that contains the matching search text. The user can switch to full view to see the entire content of the matching chapter.</p>
            </li>
            <li>Highlight search matches:
                <p>Matches in the matching documents are highlighted using a highlighting color in both snippets and full views.</p>
            </li>
        </ul>
    </div>
</div>
<div class="row landing-image-text">
    <div class="col-sm-6 col-sm-push-6">
        <img class="center-block" src="/assets/images/projects/so/1.jpg" alt="">
    </div>
    <div class="col-sm-6 col-sm-pull-6 text-justify">
        <h4 class="font-alt">Search optimization and Relevance Sorting</h4>
        <p>In this project, we analyzed an existing search engine to enhance its performance. The system was suffering from a clear slowness that negatively impacted the user experience.</p>
        <p>Our team conducted a set of time-based experiments in order to identify the source of slowness. We used real-time queries collected from the production server to conduct the time analysis. The analysis results are reported in a full analysis report that shows the results of the time experiments and resource utilization.</p>
        <p>After the analysis process, we did some changes to improve the performance and reduce the execution time. The improvements include different aspects:</p>
        <ul>
            <li>Code refactoring
                <p>Some parts of the code that cause the slowness were refactored for faster   execution time.</p>
            </li>
            <li>Parallelism
                <p>For better utilization of the available resources (multiple servers with multiple threads), we did some changes to allow independent tasks run in parallel.</p>
            </li>
            <li>Configuration changes
                <p>This includes some changes in the database and index configuration to enhance the performance.</p>
            </li>
            <li>Other changes
                <p>We recommended some changes to communication between the backend and  the frontend by isolating the slow queries in a different on-demand APIs.</p>
            </li>
        </ul>
        <p>On average, we were able to reduce the backend query time to 70% and the total response time to about 50% of the original time before the improvements. Certain queries that had been resolved in 20 seconds, took only 3 seconds of resolution time after the improvements.</p>
        <p>In addition, when the search results are sorted based on relevancy, the existing system shows the less relevant results appear before the more relevant results. We revised the existing sorting algorithm to improve the relevance sorting so that more relevant results appear ahead.</p>
    </div>
</div>


<div class="row landing-image-text pb-3 pt-0 ">
    <div class="col-sm-12 text-justify">
        <p>For sorting purposes, each matching document is given a score that represents how relevant the matching document to the user search terms. The higher the score, the more relevant the matching document is. The relevance sorting algorithm is updated to account for different factors when sorting the matching documents:</p>
        <ul>
            <li>Search fields are not equally important
                <p>One of the main issue in the old sorting of algorithm is that it handles all fields in the document equally for relevance scoring. However, it is pretty clear that some fields of the document are more important than others. For example, matches in the Title or in the Keywords are more important than matches in the affiliation.</p>
            </li>
            <li>Exclude some fields
                <p>Some fields should have a little or no contribution in relevance scoring calculations. This includes some metadata fields.</p>
            </li>
            <li>Recency/Newness
                <p>The publication year contributes to the score calculations. Matching documents with recent publication year are given a relatively higher scores than the older documents. When documents have the same scores, the newly published documents should appear in front of the older ones.</p>
            </li>
        </ul>
    </div>
</div>
