<div class="row landing-image-text">
    <div class="col-sm-6 col-sm-push-6">
        <img class="center-block" src="/assets/images/projects/ad/1.jpeg" alt="">
    </div>
    <div class="col-sm-6 col-sm-pull-6 text-justify">
        <p >In publication solutions, an author might write his name in different forms. This causes the machine to handle each name string as different author. In addition, there might be more than one author with the same name string. This issue leads to falsely merging the records of both authors. To address those issues, we designed and implemented a heuristic-based algorithm to disambiguate the authors’ names strings and identifies the real owner of a given publication.</p>
        <p>The algorithm deals with two types of name ambiguities, namely, the synonym and homonym issues. The synonym issue occurs when the same person uses different variations of his name, while the homonym issue occurs when the same exact name is shared between different individuals which is usually the case for the common names.</p>
        <p>Our algorithm uses the metadata of the publication such as affiliation, coauthors, email, etc. In addition to other extracted information as an input to do the disambiguation task. As an output, the algorithm aggregates the contributions of a given individual in one record called author profile with a unique ID.  The author profile contains different information about a particular author collected from his own publications alongside some other information such as the citation count.</p>

    </div>
</div>

<div class="row landing-image-text pt-0">
    <div class="col-sm-12 text-justify">
        <p>The nature of of disambiguation problem requires some sort of all-against-all comparisons between the author publications in order to cluster the publications of an individual in the same profile. This means that for a large dataset, we will have a tremendous number of comparisons, and each comparison encompasses a set of time-consuming calculations. To handle this performance issue and decrease the running time, we designed our algorithm to work in parallel so it can a run in a cluster of nodes each with a number of parallel threads. In addition, we enhanced the algorithm with some optimization techniques to help taking the disambiguation decision as early as possible with the minimum possible calculations.</p>
        <p>One important feature of our is the Adaptivity feature. Adaptivity means the ability of the algorithm to change its behavior based on the handled case. For example, the algorithm behaves more conservatively when processed name is a very popular one, while it becomes more lenient handling a rare name.  Similarly, the Chinese names, which are more challenging than other names due to their high level of ambiguities, are treated in a slightly different way. This change of behavior based on the case increased the accuracy and enhanced the generalization of the algorithm.</p>
        <p>In order to tune and evaluate our algorithm, we prepared different datasets that cover a wide variety of names from different places and cultures.  These datasets vary in size (small, medium and large), names origin (western names, oriental names, Chinese names etc.) and naming conventions.  Author names in these datasets are manually disambiguated by human efforts.</p>
    </div>
</div>

<div class="row landing-image-text pb-3 pt-0 ">
    <div class="col-sm-6"><img class="center-block" src="/assets/images/projects/ad/4.jpeg" alt=""></div>
    <div class="col-sm-6 text-justify">
        <p>We validated our algorithm against the collection of manually labeled reference datasets. The results showed that about 91% of the output profiles exactly matched the profiles in the reference datasets and about 8.2% partially matched the reference profiles and only less than 0.8% of error profiles.</p>
        <p>Our algorithm was applied against a real-world large database that contains more than 4 million publications with about 10.5 million name string instances including homonyms. Thanks to the parallelism and our optimization techniques, the disambiguation of all name strings in the entire database was done within 4 days on medium-size cluster. Our assessment of this process indicates that the achieved accuracy and error rates are consistent with the reference datasets results.</p>
    </div>
</div>
