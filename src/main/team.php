<?php

include 'components/skills.php';

?>
<section class="module" id="team">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h2 class="module-title font-alt">Our Team</h2>
        <div class="module-subtitle font-serif">Our core team includes several talented developers with good research and industry experience. Few of us are PhD holders, four master degree holders, and two bachelor degree holders in different computing fields. </div>
      </div>
    </div>
    <div class="row">
      <div class="row multi-columns-row">
        <div class="col-sm-6">
        <?php
            (new Skills("Research Unit Skills", ["Machine Learning", "Artificial Intelligence", "Algorithm analysis", "Design and optimization", "Semantics and Ontology Engineering"]))->getHTMLBox();
            (new Skills("Programming Languages", ["JavaScript","Python","Java","C/C++","ASP.NET","XQuery","PHP","Shell Scripting","LabVIEW"]))->getHTMLBox();
            (new Skills("XML Technologies", ["XML","DTD","Schema","XSL","XSLT","XQuery"]))->getHTMLBox();
            (new Skills("NoSQL Database Tools", ["Marklogic","XQuery","SPARQL","MongoDB"]))->getHTMLBox();
            (new Skills("Enterprise Servers", ["Marklogic","Weblogic"]))->getHTMLBox();
            (new Skills("Software Analysis and Design", ["OOA","OOD","Design Pattern","Architecture Patterns such as MVC"]))->getHTMLBox();
            (new Skills("IDE Tools", ["Eclipse", "NetBeans", "IntelliJ", "Visual Studio", "GGTS", "oXygen"]))->getHTMLBox();
            (new Skills("Deployment", ["Ant", "Maven", "Ivy", "Roxy"]))->getHTMLBox();
            
            ?>

        </div>
        <div class="col-sm-6">
          <?php
            (new Skills("Web Application Development", ["Node.js","Angular","Bootstrap","Salesforce","WordPress","Drupal","Spring","RESTFul API", "Webpack", "gulp", "Sass-LESS"]))->getHTMLBox();
            (new Skills("Java Skills", ["J2EE","J2SE","J2ME","JNI","Eclipse Plug-ins","Multi Threading", "C++-Java interfacing"]))->getHTMLBox();
            (new Skills("Image Processing and Computer Vision", ["Matlab","R","OpenCV","OpenGl"]))->getHTMLBox();
            (new Skills("Relational DBMS", ["Oracle","SQL Server", "MySql", "Derby DB"]))->getHTMLBox();
            (new Skills("Ontology and Semantic Web", ["RDF", "Turtle", "SPARQL", "Ontology Design"]))->getHTMLBox();
            (new Skills("Software Engineering Methodologies and Tools", ["Agile Scrum", "Rational Rose", "Software Architect", "RUP methodologies and UML", "Visio"]))->getHTMLBox();
            (new Skills("Version Control", ["Git", "Subversion", "CVS", "Source Safe"]))->getHTMLBox();
            (new Skills("Testing and Framework", ["JUnit", "Mockito", "PowerMock", "Stress and Stability", "Test Automation"]))->getHTMLBox();
            
          ?>

        </div>
      </div>
    </div>
  </div>
</section>