<section class="module" id="services">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h2 class="module-title font-alt">Our Services</h2>
      </div>
    </div>
    <div class="row multi-columns-row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="features-item">
          <div class="features-icon"><span class="icon-lightbulb"></span></div>
          <h3 class="features-title font-alt">Research and Development</h3>
          <p>We are a team of talented researchers and PhD holders provides solutions to complex problems in several fields, including Machine Learning, Artificial Intelligence, Image Processing, Computer Vision, NoSQL, Big Data, Semantic Web, and Ontology Engineering. Our team members keep themselves abreast of cutting-edge developments in these fields and can utilize their skills to solve challenging problems. We can build Smart systems and model your data to follow the latest standards.</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="features-item">
          <div class="features-icon"><span class="icon-mobile"></span></div>
          <h3 class="features-title font-alt">Web and Mobile Application</h3>
          <p>The team has expertise in several Web and Mobile technologies.  The team can provide in time deliveries with high quality. Part of the quality is to keep the application open for extensibility and be designed for low maintenance and evolution cost.</p>
        </div>
      </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-puzzle"></span></div>
                  <h3 class="features-title font-alt">Algorithm Development</h3>
                  <p>The team is experienced in algorithm design and optimization. Our main area of expertise is in Search Engine Optimization and query optimization. The team has proven skills in NoSQL data remodeling to achieve significant improvement in query resolution time. If you have complex problems or inefficient algorithms, we can help you.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-genius"></span></div>
                  <h3 class="features-title font-alt">Data Science</h3>
                  <p>The team includes scientists and developers those are experienced in several fields of data science including data analysis, data clean up and processing, information discovery and retrieval, clustering, classification, data modeling and design. We can provide you with educated suggestions and solutions about your data model, best approaches for analysis and manage your data, and best approaches to achieve data summaries and discover information and patterns hidden in the data.</p>
                </div>
              </div>
            </div>
          </div>
        </section>