<footer class="footer bg-dark">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <p class="copyright font-alt">&copy; <?= date("Y"); ?>&nbsp;<a href="/"><?php echo $SYSTEM_VARIABLES['company-name'];?></a>, All Rights Reserved</p>
      </div>
    
    </div>
  </div>
</footer>