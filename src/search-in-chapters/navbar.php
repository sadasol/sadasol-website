<nav class="navbar navbar-custom navbar-transparent navbar-fixed-top one-page" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            
            <a class="navbar-brand" href="/">
            <img src="/assets/images/logo.png" class="pull-left"/>
            <?= $SYSTEM_VARIABLES['company-name'];?>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Home</a></li>
                <li><a class="section-scroll" href="#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>