<section class="module" id="works">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Our Works</h2>
                <div class="module-subtitle font-serif"></div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <ul class="filter font-alt" id="filters">
                  <li><a class="current wow fadeInUp" href="#" data-filter="*">All</a></li>
                  <li><a class="wow fadeInUp" href="#" data-filter=".algorithms" data-wow-delay="0.2s">Algorithms</a></li>
                  <li><a class="wow fadeInUp" href="#" data-filter=".webapp" data-wow-delay="0.4s">Web Application</a></li>
                  <li><a class="wow fadeInUp" href="#" data-filter=".webservice" data-wow-delay="0.6s">Web Service</a></li>
                  <li><a class="wow fadeInUp" href="#" data-filter=".searchopt" data-wow-delay="0.6s">Search Optemization</a></li>
                </ul>
              </div>
            </div>
          </div>
          <ul class="works-grid works-grid-gut works-grid-3 works-hover-w" id="works-grid">
            <li class="work-item algorithms webapp"><a href="/author-disambiguation">
                <div class="work-image"><img src="/assets/images/projects/ad.jpeg" alt="Author disambiguation"/></div>
                <div class="work-caption font-alt">
                  <h3 class="work-title">Author disambiguation</h3>
                  <div class="work-descr">Author disambiguation solgan .....</div>
                </div></a></li>
            <li class="work-item algorithms webservice"><a href="/recommendation-service">
                <div class="work-image"><img src="/assets/images/projects/rs.jpeg" alt="Recommendation service"/></div>
                <div class="work-caption font-alt">
                  <h3 class="work-title">Recommendation service</h3>
                  <div class="work-descr">Recommendation service slogan .....</div>
                </div></a></li>
            <li class="work-item searchopt algorithms webservice"><a href="/search-in-chapters">
                <div class="work-image"><img src="/assets/images/projects/so.jpg" alt="Portfolio Item"/></div>
                <div class="work-caption font-alt">
                  <h3 class="work-title">Search in Books Chapters</h3>
                  <div class="work-descr">Search APIs slogam ......</div>
                </div></a></li>
            </ul>
        </section>