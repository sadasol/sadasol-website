
<div class="row landing-image-text">
    <div class="col-sm-6 col-sm-push-6 pb-3 ">
        <img class="center-block" src="/assets/images/projects/rs/1.jpg" alt="">
    </div>
    <div class="col-sm-6 col-sm-pull-6 text-justify">
        <p>The article recommendation service finds the top similar articles to the one that the user is currently reading. The recommendation service is a typical service that appears in many content websites to suggest the user what he might be interested to read next. In a nutshell, the recommendation algorithm accepts an ID of a given article, extract the important information from that particular article, and then consults a large database to determine the top similar articles.</p>
        <p>The extracted information from the input article such as the title, abstract and keywords are used to search for similar articles. The algorithm uses a weighting system to determine the significance of each feature and its contribution in the recommendation algorithm. For instance, if the title is given a higher weight, the information extracted from the title will have a higher impact on the similarity calculations</p>
        <p>To help our client to tune the algorithm to perfectly match their business logic using the different available configuration parameters, we developed a UI application that communicates with the underlying recommendation service using a restful API. The user can change the tuning parameters interactively and see the effect on the returned related similar articles and their order.</p>
    </div>
</div>
