<!DOCTYPE html>
<html lang="en-US" dir="ltr">   
  <?php 
    
    include '../lib/system-variables.php'; 
    include '../sections/head.php'; 
    
  ?>
  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
      <?php 
        include('../components/loader.php'); 
        include('navbar.php');
        include('banner.php');
      ?>
    
      <div class="main">
        <div class="container">
          <?php
            include('rows.php');
          ?>
        </div>
        <?php

         
          include('../sections/contact.php');
          include('../components/devider-w.php');
          include('../sections/footer.php');
        ?>
  
      </div>
      <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>
    <?php include('../sections/end-js-links.php')?>
  </body>
</html>